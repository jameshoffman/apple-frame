//
// Pins
//

const int RED_PIN = 3; // Uno = 6;
const int GREEN_PIN = 6; // Uno = 3;
const int BLUE_PIN = 5; // Uno = 5;

const int BUTTON_PIN = 2;

const int SWITCH_MODE_CHROMA_PIN = 7; // Uno = 12;
const int SWITCH_MODE_SINGLE_PIN = 8; // Uno = 13;

//
// Data types
//

typedef struct RGBColor {
  int red;
  int green;
  int blue;
} RGBColor;

typedef struct HSVColor {
  int hue;
  float saturation;
  float value;
} HSVColor;

//
// Constants
//

const unsigned int CHROMA_TARGET_ELAPSED_TIME = 20;
const unsigned int COLOR_CHANGE_TARGET_ELAPSED_TIME = 5;

const unsigned int BUTTON_DEBOUNCE_DELAY = 50;

const RGBColor SINGLE_COLOR_MODES_COLORS[] = {
  {.red = 200, .green = 200, .blue = 255 }, /*white-ish, tweaked to look less purple*/
  {.red = 255, .green = 0,   .blue = 0 },
  {.red = 0,   .green = 255, .blue = 0 },
  {.red = 0,   .green = 0,   .blue = 255 }
};

//
// Variables
//

RGBColor ledColor = {.red = 0, .green = 0, .blue = 0 };

unsigned long previousTime = 0;
HSVColor currentChromaColor = { 0 };

int mode = 0;
int previousMode = -1;

unsigned long previousButtonDebounceTime = 0;
int previousButtonState = LOW;
int buttonState = LOW;


//
// Setup
//

void setup() {
  Serial.begin(9600);

  pinMode(RED_PIN, OUTPUT);
  pinMode(GREEN_PIN, OUTPUT);
  pinMode(BLUE_PIN, OUTPUT);

  pinMode(BUTTON_PIN, INPUT);

  pinMode(SWITCH_MODE_CHROMA_PIN, INPUT);
  pinMode(SWITCH_MODE_SINGLE_PIN, INPUT);
}

//
// Loop
//

void loop() {
  unsigned long currentTime = millis();
  unsigned long elapsedTime = currentTime - previousTime;

  bool isChromaMode = (digitalRead(SWITCH_MODE_CHROMA_PIN) == HIGH);

  if (isChromaMode) {
    //
    // Chroma mode
    //

    if (elapsedTime >= CHROMA_TARGET_ELAPSED_TIME) {
      if (currentChromaColor.value == 0.0f) {
        // first iteration of the mode change
        // compute current HSV value to smoothly transition from previous color
        currentChromaColor = RGBtoHSV(ledColor);
        currentChromaColor.saturation = 1.0f;
        currentChromaColor.value = 1.0f;
      }

      currentChromaColor.hue++;
      currentChromaColor.hue = (currentChromaColor.hue % 360); // [0, 359]

      ledColor = HSVtoRGB(currentChromaColor);
      
      previousTime = millis();
      previousMode = mode;
    }
  } else {
    //
    // Single color mode
    //

    // reset chroma value
    currentChromaColor = { 0 };
    

    bool buttonPressed = false;

    // Debounce: https://www.arduino.cc/en/Tutorial/Debounce
    int buttonStateReading = digitalRead(BUTTON_PIN);
    
    if (buttonStateReading != previousButtonState) {
      previousButtonDebounceTime = millis();
    }

    if ((millis() - previousButtonDebounceTime) > BUTTON_DEBOUNCE_DELAY) {
      // if the button state has changed:
      if (buttonStateReading != buttonState) {
        buttonState = buttonStateReading;
        
        if (buttonState == HIGH) {
          // Button pressed
          buttonPressed = true;
        }
      }
    }

    previousButtonState = buttonStateReading;
    // end debounce

    if (buttonPressed) {
      mode++;
      mode = mode % 4; // make sure mode is always within 0...3
    }
    
    if (elapsedTime >= COLOR_CHANGE_TARGET_ELAPSED_TIME || buttonPressed) {
      RGBColor newColor = SINGLE_COLOR_MODES_COLORS[mode];
  
      ledColor.red += computeColorChangeStep(ledColor.red, newColor.red);
      ledColor.green += computeColorChangeStep(ledColor.green, newColor.green);
      ledColor.blue += computeColorChangeStep(ledColor.blue, newColor.blue);

      previousTime = millis();
      previousMode = mode;
    }
  }

  writeLedColor();
}

void writeLedColor() {
  analogWrite(RED_PIN, ledColor.red);
  analogWrite(GREEN_PIN, ledColor.green);
  analogWrite(BLUE_PIN, ledColor.blue);
}

int computeColorChangeStep(int startValue, int endValue) {
  if (startValue == endValue) {
    return 0;
  } else if (startValue < endValue) {
    return 1;
  } else {
    return -1;
  }
}

//
// RGB and HSV conversion utils
//

RGBColor HSVtoRGB(HSVColor hsvColor) {
  // https://en.wikipedia.org/wiki/HSL_and_HSV#From_HSV

  float chroma  = hsvColor.value * hsvColor.saturation;
  float range = hsvColor.hue / 60.0f;

  float colorApproximation = chroma * (1.0f - fabs(fmod(range, 2.0f) - 1.0f));
  float red = 0;
  float green = 0;
  float blue = 0;

  if (0 <= range && range < 1.0) {
    red = chroma;
    green = colorApproximation;
  } else if (1.0 <= range && range < 2.0) {
    green = chroma;
    red = colorApproximation;
  } else if (2.0 <= range && range < 3.0) {
    green = chroma;
    blue = colorApproximation;
  } else if (3.0 <= range && range < 4.0) {
    blue = chroma;
    green = colorApproximation;
  } else if (4.0 <= range && range < 5.0) {
    blue = chroma;
    red = colorApproximation;
  } else if (5.0 <= range && range < 6.0) {
    red = chroma;
    blue = colorApproximation;
  }
  // else black/greyscale

  float colorAmount = hsvColor.value - chroma;

  red += colorAmount;
  green += colorAmount;
  blue += colorAmount;

  // Convert to [0, 255]
  RGBColor rgbColor = { .red = (int)(red * 255), .green = (int)(green * 255), .blue = (int)(blue * 255) };

  return rgbColor;
}

HSVColor RGBtoHSV(RGBColor rgbColor) {
  // http://coecsl.ece.illinois.edu/ge423/spring05/group8/finalproject/hsv_writeup.pdf

  HSVColor hsvColor = { .hue = 0, .saturation = 0, .value = 0 };

  float minColor = min(min(rgbColor.red, rgbColor.green), rgbColor.blue);
  float maxColor = max(max(rgbColor.red, rgbColor.green), rgbColor.blue);
  float deltaColor = maxColor - minColor;

  if (maxColor != 0) {
    hsvColor.value = maxColor / 255; // to bring valeu in range [0, 1.0]

    hsvColor.saturation = deltaColor / maxColor;

    if (rgbColor.red == maxColor) {
      hsvColor.hue = (float)(rgbColor.green - rgbColor.blue) / deltaColor;
    } else if (rgbColor.green == maxColor) {
      hsvColor.hue = 2 + ((float)(rgbColor.blue - rgbColor.red) / deltaColor);
    } else {
      hsvColor.hue = 4 + ((float)(rgbColor.red - rgbColor.green) / deltaColor);
    }

    hsvColor.hue *= 60;

    if (hsvColor.hue < 0) {
      hsvColor.hue += 360;
    }
  }
  // else color was black

  return hsvColor;
}
